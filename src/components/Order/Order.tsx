import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { Input } from '../Input/Input';
import { Map } from '../Map/Map';

export const Order = () => {

  const [isFirstSubmit, setIsFirstSubmit] = useState(true);

  const [address, setAddress] = useState('');
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setAddress(event.target.value);
    console.log(event);
  };

  const [errors, setErrors] = useState<{ errorText: string } | null>(null);
  useEffect(() => {
    if (isFirstSubmit) return;
    setErrors(() => {
      if (!address) {
        return {
          errorText: 'Поле обязательно для заполнения',
        };
      }
      return null;
    });
  }, [address]);

  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setIsFirstSubmit(false);
    if (errors) return;
    console.log('submit');
  };

  return (
    <div className='order'>
      <h1 className='h1 order__title'>Детали заказа</h1>
      <form className='order__form' noValidate={true} onSubmit={onSubmit}>
        <div className='order__info'>
          <span>Откуда</span>
          <Input
            name='name'
            type='text'
            placeholder='Улица, номер дома'
            required={true}
            onChange={handleChange}
            value={address}
            hasError={!!errors}
            errorText={errors?.errorText}
          />
          <span>Подходящий экипаж</span>
          <div className='order__card'>asd</div>
        </div>
        <div className='order__grid'>
          <Map />
          <div className='order__car' />
        </div>
        <button type='submit' className='order__button'>Заказать</button>
      </form>
    </div>
  );
};
