import React, { ChangeEventHandler } from 'react';

export interface IInput {
  name: string,
  type: string,
  placeholder?: string,
  required: boolean,
  onChange: ChangeEventHandler<HTMLInputElement>,
  value: string,
  hasError: boolean,
  errorText?: string
}

export const Input = (props: IInput) => {
  const {
    name,
    type,
    placeholder = '',
    required,
    onChange,
    value,
    hasError,
    errorText,
  } = props;

  return (
    <label className={`input${value ? ' input_filled' : ''}${hasError ? ' error' : ''}`}>
      <input
        className='input__field'
        name={name}
        type={type}
        required={required}
        onChange={onChange}
        value={value}
      />
      <span className='input__placeholder'>{placeholder}</span>
      {hasError &&
        <span className='input__error-text'>{errorText}</span>
      }
    </label>
  );
};
