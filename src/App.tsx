import React from 'react';
import { Order } from './components/Order/Order';

export const App = () => (
  <div className='App'>
    <div className='container'>
      <Order />
    </div>
  </div>
);
