import { useEffect, useState } from 'react';

export const useYandexMap = () => {
  const [map, setMap] = useState(null);
  useEffect(() => {
    let myMap = null;
    window.ymaps.ready(() => {
      myMap = new window.ymaps.Map('map', {
        center: [55.76, 37.64],
        zoom: 7,
      });
      setMap(myMap);
    });
  }, []);

  return [map];
};
